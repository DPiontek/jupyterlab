FROM ubuntu:18.04

# INSTALL s6-overlay
# http://www.greg-gilbert.com/2017/05/basic-setup-for-s6-and-s6-overlay-in-docker/
# https://github.com/just-containers/s6-overlay/releases/download/v1.22.1.0/s6-overlay-amd64.tar.gz
ARG S6_VERSION=v1.22.1.0
RUN DEBIAN_FRONTEND=noninteractive apt-get update &&\
    DEBIAN_FRONTEND=noninteractive apt-get install --yes --install-recommends wget &&\
    wget --no-check-certificate -O /tmp/s6-overlay-amd64.tar.gz https://github.com/just-containers/s6-overlay/releases/download/${S6_VERSION}/s6-overlay-amd64.tar.gz && \
    tar zxf /tmp/s6-overlay-amd64.tar.gz -C / &&\
    rm /tmp/s6-overlay-amd64.tar.gz &&\
    DEBIAN_FRONTEND=noninteractive apt-get purge --yes wget &&\
    rm -f ~/.wget-hsts &&\
    DEBIAN_FRONTEND=noninteractive apt-get autoremove --yes &&\
    DEBIAN_FRONTEND=noninteractive apt-get clean &&\
    rm -rf /var/lib/apt/lists/*
CMD /init

# INSTALL miniconda
ARG CONDA_HOME="/opt/conda"
ARG CONDA_VERSION=4.7.12.1
ENV CONDA_HOME="${CONDA_HOME}"
RUN DEBIAN_FRONTEND=noninteractive apt-get update &&\
    DEBIAN_FRONTEND=noninteractive apt-get install --yes --no-install-recommends wget &&\
    wget --no-check-certificate -O /tmp/Miniconda3-Linux-x86_64.sh https://repo.anaconda.com/miniconda/Miniconda3-${CONDA_VERSION}-Linux-x86_64.sh && \
    chmod 755 /tmp/Miniconda3-Linux-x86_64.sh &&\
    /tmp/Miniconda3-Linux-x86_64.sh -b -p ${CONDA_HOME} &&\
    DEBIAN_FRONTEND=noninteractive apt-get purge --yes wget &&\
    rm -f ~/.wget-hsts &&\
    DEBIAN_FRONTEND=noninteractive apt-get autoremove --yes &&\
    DEBIAN_FRONTEND=noninteractive apt-get clean &&\
    rm -rf /var/lib/apt/lists/*

# INSTALL jupyter lab
ARG NOTEBOOK_DIR=/root
ENV NOTEBOOK_DIR=${NOTEBOOK_DIR}
ADD services.d/jupyterlab/ /etc/services.d/jupyterlab/
RUN ${CONDA_HOME}/bin/conda create -n jupyterlab jupyterlab &&\
    ${CONDA_HOME}/bin/conda install --yes -n jupyterlab -c conda-forge ipywidgets nodejs &&\
    ${CONDA_HOME}/bin/conda run -n jupyterlab jupyter labextension install @jupyter-widgets/jupyterlab-manager &&\
    chmod 755 /etc/services.d/jupyterlab/run &&\
    ${CONDA_HOME}/bin/conda run -n jupyterlab jupyter kernelspec remove -f python3


# INSTALL APP ENVIRONMENT
# https://ipython.readthedocs.io/en/stable/install/kernel_install.html#kernels-for-different-environments
ADD app_requirements.txt ${CONDA_HOME}/envs
RUN ${CONDA_HOME}/bin/conda create -n app ipykernel --file ${CONDA_HOME}/envs/app_requirements.txt &&\
    ${CONDA_HOME}/bin/conda run -n app python -m ipykernel install \
        --prefix=${CONDA_HOME}/envs/jupyterlab \
        --name app\
        --display-name "$(${CONDA_HOME}/bin/conda run -n app python --version)"

# INSTALL R ENVIRONMET
# http://richpauloo.com/2018-05-16-Installing-the-R-kernel-in-Jupyter-Lab/
ADD r_requirements.txt ${CONDA_HOME}/envs
RUN DEBIAN_FRONTEND=noninteractive apt-get update &&\
    DEBIAN_FRONTEND=noninteractive apt-get install --yes --no-install-recommends libxrender1 libxext6 &&\
    DEBIAN_FRONTEND=noninteractive apt-get autoremove --yes &&\
    DEBIAN_FRONTEND=noninteractive apt-get clean &&\
    rm -rf /var/lib/apt/lists/* &&\
    ${CONDA_HOME}/bin/conda create -n r r-irkernel --file ${CONDA_HOME}/envs/r_requirements.txt &&\
    PATH=/opt/conda/envs/jupyterlab/bin:${PATH} ${CONDA_HOME}/bin/conda run -n r Rscript -e "IRkernel::installspec(\
        name = 'ir', \
        user = FALSE, \
        displayname = '$(${CONDA_HOME}/bin/conda run -n r Rscript -e 'R.Version\(\)\$version.string' | cut -f2 -d\]|sed -e 's/"//g'| sed -e 's/^ *//g')'\
        )"

# INSTALL SSHD
ADD ssh /etc/ssh/
ADD services.d/sshd/ /etc/services.d/sshd/
RUN DEBIAN_FRONTEND=noninteractive apt-get update &&\
    DEBIAN_FRONTEND=noninteractive apt-get install --yes --no-install-recommends openssh-server &&\
    DEBIAN_FRONTEND=noninteractive apt-get autoremove --yes &&\
    DEBIAN_FRONTEND=noninteractive apt-get clean &&\
    rm -rf /var/lib/apt/lists/* &&\
    chmod 755 /etc/services.d/sshd/run &&\
    chmod 644 /etc/ssh/sshd_config &&\
    rm /etc/ssh/sshd_config.ucf-dist &&\
    mkdir /run/sshd

